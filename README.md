# Sri Isopanishad

This project is used as a proof of concept for continuous integration process using docker.

## Introduction

We are building a static web site using a single page, that is index.html.


### Objetives

Demonstrate how to use a CI process to build and release a static site container.

## Requirements

You have to install docker-ce on your development and build machine.

## Build

```shell
$ docker build -t static-site-sample:v1 .
```

## Testing

Lets check the status of new image:

```shell
$ docker images
```

Now run a new container using this image:

```shell
$ docker run -d -p 80:80 SriIsopanishad:v1
```

And check if is running:

```shell
$ docker ps
CONTAINER ID        IMAGE                   COMMAND                  CREATED             STATUS              PORTS                NAMES
522e9988be06        static-site-sample:v1   "nginx -g 'daemon of…"   4 seconds ago       Up 2 seconds        0.0.0.0:80->80/tcp   gracious_franklin
```

And thest the web page:

```shell
$ curl http://localhost:80
```

## Clean

To start over a clean environment:

```shell
$ docker stop 522e9988be06
```

And remove the container:

```shell
$ docker rm 522e9988be06
```

And remove the image:

```shell
$ docker rmi a6668827ce95
```

## References

TODO.
